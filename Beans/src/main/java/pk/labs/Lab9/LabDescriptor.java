package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;
import pk.labs.Lab9.beans.impl.ConsultationBean;
import pk.labs.Lab9.beans.impl.ConsultationListBean;
import pk.labs.Lab9.beans.impl.ConsultationListFactoryBean;
import pk.labs.Lab9.beans.impl.TermBean;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermBean.class;
    public static Class<? extends Consultation> consultationBean = ConsultationBean.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationListBean.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationListFactoryBean.class;
    
}
