/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class ConsultationListBean implements ConsultationList, Serializable {

    private final Date consultationStart = new Date(2013, 9, 1);
    private final Date consultationEnd = new Date(2014, 0, 27);
    private final int minConsultationTime = 15;
    private final int maxConsultationTime = 60;
    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    private List<Consultation> consultationList;

    public ConsultationListBean() {
        this.consultationList = new ArrayList<Consultation>();
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        this.vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        this.vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public int getSize() {
        return this.consultationList.size();
    }

    @Override
    public Consultation[] getConsultation() {
        return this.consultationList.toArray(new Consultation[this.consultationList.size()]);
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.consultationList.get(index);
    }

    @Override
    public void addConsultation(Consultation newConsultation) throws PropertyVetoException {
        //int duration = (int) ((newConsultation.getEndDate().getTime() - newConsultation.getBeginDate().getTime()) / 60000); // 1000 * 60(ms) = 1min
        //Term newTerm = new TermBean(newConsultation.getBeginDate(), duration);

        //if (isConsultationAvailable(newTerm)) {
        if (consultationCheck(newConsultation)) {
            ((ConsultationBean) newConsultation).addVetoableChangeListener(new ConsultationBeanListener(this.consultationList));
            List<Consultation> newList = new ArrayList<Consultation>();
            newList.addAll(this.consultationList);
            newList.add(newConsultation);
            this.vetoableChangeSupport.fireVetoableChange("consultation", this.consultationList, newList);
            this.consultationList.add(newConsultation);
            this.propertyChangeSupport.firePropertyChange("consultation", null, this.consultationList);
        } else {
            throw new PropertyVetoException("Niepoprawny termin konsultacji", null);
        }
    }

    private boolean consultationCheck(Consultation consultation) {
        if (!(consultation.getBeginDate().getTime() >= this.consultationStart.getTime() || consultation.getEndDate().getTime() <= this.consultationEnd.getTime())) {
            //return false;
            System.out.println("Data poza ograniczeniem");
        }

        int duration = (int) ((consultation.getEndDate().getTime() - consultation.getBeginDate().getTime()) / 60000);
        if (duration <= this.minConsultationTime || duration >= this.maxConsultationTime) {
            //return false;
            System.out.println("Czas trwania poza ograniczeniem");
        }

        return true;
    }
/*
    private boolean isConsultationAvailable(Term nowyTermin) {

        boolean wykladowcaZajety = true;

        for (Consultation konsultacja : this.consultationList) {
            int sprBegin = konsultacja.getBeginDate().compareTo(nowyTermin.getBegin());
            int sprEnd;

            if (sprBegin == 0) { //takie same rozpoczęcie konsultacji, nie można dodać
                wykladowcaZajety = false;
                return wykladowcaZajety;
            }

            if (sprBegin < 0) {
                sprEnd = konsultacja.getEndDate().compareTo(nowyTermin.getBegin()); //sprawdzam brak kolizji z końcem konsultacji 'c'
                if (sprEnd > 0) {
                    wykladowcaZajety = false;
                    break;
                }
            } else if (sprBegin > 0) {
                sprEnd = nowyTermin.getEnd().compareTo(konsultacja.getBeginDate()); //sprawdzam brak kolizji z końcem konsultacji 'consultation'
                if (sprEnd > 0) {
                    wykladowcaZajety = false;
                    break;
                }
            }
        }
        return wykladowcaZajety;
    }*/
}
