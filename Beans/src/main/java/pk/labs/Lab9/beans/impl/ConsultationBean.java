/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class ConsultationBean implements Consultation, Serializable {

    private String student;
    private Term term;

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        this.vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        this.vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    public ConsultationBean() {
        this.term = new TermBean();
    }

    public ConsultationBean(String student, Term term) {
        this.student = student;
        this.term = term;
    }

    @Override
    public String getStudent() {
        return this.student;
    }
    ///////////////////////////////////
    public Term getTerm() {
        return this.term;
    }
    ///////////////////////////////////
    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        this.vetoableChangeSupport.fireVetoableChange("term", this.term, term);
        this.term = term;
        this.propertyChangeSupport.firePropertyChange("term", this.term, term);
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
        if (minutes > 0) {
            TermBean newValue = new TermBean(this.term.getBegin(), this.term.getDuration() + minutes);
            this.vetoableChangeSupport.fireVetoableChange("term", this.term, newValue);
            this.term.setDuration(this.term.getDuration() + minutes);
            this.propertyChangeSupport.firePropertyChange("term", this.term, newValue);
        }
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }
}
