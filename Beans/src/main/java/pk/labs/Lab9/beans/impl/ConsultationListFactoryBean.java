/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author st
 */
public class ConsultationListFactoryBean implements ConsultationListFactory {

    public ConsultationListFactoryBean() {

    }

    @Override
    public ConsultationList create() {
        return new ConsultationListBean();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        ConsultationList list = create();
        if (deserialize) {
            try {
                XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("ConsultationList.xml")));
                Consultation[] array = (Consultation[]) decoder.readObject();
                if (array == null) {
                    return new ConsultationListBean();
                }
                
                for (Consultation consultation : array) {
                    list.addConsultation(consultation);
                }
                decoder.close();
                return list;
            } catch (Exception ex) {
                System.out.println("LOAD ERROR");
                return new ConsultationListBean();
            }
        } else {
            return list;
        }
    }

    @Override
    public void save(ConsultationList consultationList) {
        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("ConsultationList.xml")));
            encoder.writeObject(consultationList.getConsultation());
            encoder.close();
        } catch (Exception ex) {
            System.out.println("SAVE ERROR");
        }
    }
}
