/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class ConsultationBeanListener implements VetoableChangeListener {

    private final List<Consultation> list;

    public ConsultationBeanListener(List<Consultation> list) {
        this.list = list;
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        Term oldTerm = (Term) evt.getOldValue();
        Term newTerm = (Term) evt.getNewValue();


        for (Consultation con : list) {
            if (con.getBeginDate().getTime() != newTerm.getBegin().getTime()) {
                if (newTerm.getBegin().getTime() > con.getBeginDate().getTime() && newTerm.getBegin().getTime() < con.getEndDate().getTime()) {
                    throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
                }

                if (newTerm.getEnd().getTime() > con.getBeginDate().getTime() && newTerm.getEnd().getTime() < con.getEndDate().getTime()) {
                    throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
                }

                if (newTerm.getBegin().getTime() < con.getBeginDate().getTime() && newTerm.getEnd().getTime() > con.getEndDate().getTime()) {
                    throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
                }
            }
        }

        /*
         for (int i = 0; i < this.list.size(); i++) {
         Consultation consultation = this.list.get(i);
         if (consultation.getBeginDate().getTime() == oldTerm.getBegin().getTime()) {
         if (i != this.list.size() - 1 && this.list.get(i + 1).getBeginDate().getTime() < newTerm.getEnd().getTime()) {
         throw new PropertyVetoException("Nie mozna przedluzyc konsultacji!", evt);
         }
         }
         }*/
    }
}
