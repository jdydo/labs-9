/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class TermBean implements Term, Serializable {

    private Date begin;
    private Date end;
    private int duration;

    public TermBean() {
        this.begin = Calendar.getInstance().getTime();
        this.end = new Date();
        this.end.setTime(begin.getTime());
        this.duration = 0;
    }

    public TermBean(Date begin, Date end) {
        this.begin = begin;
        this.end = end;
        long time = (end.getTime() - begin.getTime()) / 60000;
        this.duration = (int) time;
    }

    public TermBean(Date begin, int duration) {
        this.begin = begin;
        this.duration = duration;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(begin);
        calendar.add(Calendar.MINUTE, duration);
        this.end = calendar.getTime();
    }

    @Override
    public Date getBegin() {
        return this.begin;
    }

    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration > 0) {
            this.duration = duration;
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(begin);
            calendar.add(Calendar.MINUTE, duration);
            this.end = calendar.getTime();
        }
    }

    @Override
    public Date getEnd() {
        return this.end;
    }
}
