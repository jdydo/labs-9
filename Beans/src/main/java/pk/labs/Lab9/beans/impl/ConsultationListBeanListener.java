/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;

/**
 *
 * @author st
 */
public class ConsultationListBeanListener implements VetoableChangeListener {

    //Consultation[] consultations;

    public ConsultationListBeanListener() {
            //Consultation[] consultations) {
        //this.consultations = consultations;
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        List<Consultation> newList = (List<Consultation>) evt.getNewValue();
        List<Consultation> oldList = (List<Consultation>) evt.getOldValue();
        
        Consultation consultation = newList.get(newList.size()-1);
        
        for (Consultation con : oldList) {
            if (consultation.getBeginDate().getTime() > con.getBeginDate().getTime() && consultation.getBeginDate().getTime() < con.getEndDate().getTime() ) {
                throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
            }
            
            if (consultation.getEndDate().getTime() > con.getBeginDate().getTime() && consultation.getEndDate().getTime() < con.getEndDate().getTime() ) {
                throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
            }
            
            if (consultation.getBeginDate().getTime() < con.getBeginDate().getTime() && consultation.getEndDate().getTime() > con.getEndDate().getTime() ) {
                throw new PropertyVetoException("Nie mozna dodac konsultacji!", evt);
            }

        }
        
    }
}